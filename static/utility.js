
function inet_ntoa(ipNumber){
  var ipString = "";
  for (var i = 0; i < 4; ++i){
    var n = ipNumber % 256;
    ipString += "." + n.toString();
    ipNumber = Math.floor(ipNumber / 256);
  }
  return ipString.substring(1);
}

function uint32B2L(num){
  if (num >= Math.pow(2, 32)){
    return NaN;
  }

  var ret = 0; 
  var bytes = new Array();
  for (var i = 0; i < 4; ++i){
    ret += (num % 256) * Math.pow(256, 3 - i);
    num = Math.floor(num / 256);
  }
  return ret;
}

/*
  replace "\n" with "<br />"
*/
function adjustText(text){
  var newText = text.replace(new RegExp("\n", "gm"), "<br />");
  if (newText.substr(0, 6) == "<br />"){
    newText = newText.substr(6);
  }
  return newText;
}
