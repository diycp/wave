<?php

/**
 * 异常。
 */

/**
 * 找不到资源处理函数。
 */
class HandlerNotFoundException extends Exception {
  function __construct($message = "", $code = 0, $previous = NULL){
    parent::__construct($message, $code, $previous);
  }
}

/**
 * 找不到模板。
 */
class TemplateNotFoundException extends Exception {
  function __construct($message = "", $code = 0, $previous = NULL){
    parent::__construct($message, $code, $previous);
  }
}

/**
 * 连接数据库失败。
 */
class FailToConnectDbException extends Exception {
  function __construct($message = "", $code = 0, $previous = NULL){
    parent::__construct($message, $code, $previous);
  }
}

?>
