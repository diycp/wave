<?php
/**
 * Utility.php
 * 实用函数库。
 */

/**
 * 显示错误信息并终止PHP脚本执行。
 */
function error($message, $file, $line){
  $html = "<!DOCTYPE html><head><title>Error: "
    .$message."</title></head><body><h1>Error</h1><p>"
    ."file: ".$file." line: ".$line."</p><p>".$message
    ."</p></body>";
  die($html);
}

/**
 * 显示PHP对象内容。
 */
function dump($object){
  echo "<h3>dump: ".gettype($object)."</h3><div align=left><pre>\n".
  htmlspecialchars(print_r($object, true))."\n</pre></div>\n";
}

/**
 * 默认的异常处理函数。
 */
function exceptionHandler($exception){
  dump($exception);
}

/**
 * 根据资源后缀判断MIME类型。
 */
function getMimeBySuffix($suffix){
  /** @todo 增加MIME类型 */
  $mimeTable = array(
    '.css' => 'text/css',
    '.js' => 'application/javascript',
  );
  $defaultMime = 'application/octet-stream';
  if (in_array($suffix, $mimeTable)){
    return $defaultMime;
  } 
  return $mimeTable[$suffix];
}

/**
 * 处理静态资源的函数。
 */
function staticHandler($user, $parameters, $app){
  $resourceId = $app->getResourceId();
  header('Content-type: '.getMimeBySuffix(getFileSuffix($resourceId)));
  $content = file_get_contents($app->getStaticDir().$resourceId);
  $app->getSmarty()->assign("content", $content);
}

/**
 * 获取文件后缀名，包括“.”。
 */
function getFileSuffix($filename){
  $pos = strrpos($filename, '.');
  if (!$pos){
    return '';
  }
  return substr($filename, $pos);
}

?>
