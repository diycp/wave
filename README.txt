wave
简单PHP框架。

理念：
  1 把网页服务器看成一个映射：
    webserver(user, httpMethod, resourceId, parameters) -> (mimeType, String)
  2 保留URL中的路径语义。

Nginx配置：
  if ($request_filename != /wave/index.php){
    rewrite /wave/* /wave/index.php;
  }

