<?php

function handler_page2($user, $parameters, $app){
    $app->invokeHandler('pagenav', $user, $parameters, $app);
    $smarty = $app->getSmarty();
    $smarty->assign('msg', 'It Works');
    $smarty->assign('content', 'page2');
    $smarty->display('page.html');
    return true;
}

?>
