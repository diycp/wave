<?php

function handler_page1($user, $parameters, $app){
    $app->invokeHandler('pagenav', $user, $parameters, $app);
    $smarty = $app->getSmarty();
    $smarty->assign('msg', 'It Works');
    $smarty->assign('content', 'page1');
    $smarty->display('page.html');
    return true;
}

?>
