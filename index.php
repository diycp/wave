<?php

if (!defined('BASEPATH')) define('BASEPATH', dirname(__FILE__).'/');

require_once(BASEPATH.'framework/Config.php');
require_once(BASEPATH.'framework/Application.php');

$app = new Application();
$app->run();

?>
